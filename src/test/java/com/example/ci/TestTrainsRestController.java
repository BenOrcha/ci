package com.example.ci;


import com.example.ci.web.TrainsRestController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TrainsRestController.class)
public class TestTrainsRestController {

    @MockBean
    TrainsInterface trainsInterface;

    @Autowired
    MockMvc mvc;

    @Test
    public void testChercherTrainParLieuDeDepart() throws Exception {
        mvc.perform(get("/trains")
                        .param("nomVille", "Paris"))
                        .andDo(print())
                        .andExpect(status().isOk());
    }
}
