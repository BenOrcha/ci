package com.example.ci;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public interface TrainsInterface {

    public ArrayList<String> listeDesGares();

    /**
     * Retourne la liste des trains ayant pour gare de départ nomVille
     * @param nomVille
     * @return liste de gares
     * @throws Exception si nomVille est nulle
     */
    public ArrayList<Train> chercherTrainParLieuDeDepart(String nomVille) throws Exception;

    /**
     * Retourne la liste des trains ayant pour gare d'arrivée nomVille
     * @param nomVille
     * @return liste de gares
     * @throws Exception si nomVille est nulle
     */
    public ArrayList<Train> chercherTrainParLieuDArrivee(String nomVille) throws Exception;

    /**
     * Retourne la liste des trains ayant pour ville de départ nomVilleDeDepart,
     * pour ville d'arrivée nomVilleArrivee,
     * et qui partent après la date dateDeDepart
     * @param nomVilleDeDepart
     * @param nomVilleArrivee
     * @param dateDeDepart au format hh:mm jj/mm/aaaa, exemple: "23:59 01/01/2021")
     * @throws Exception si un des paramètres est nul
     * @return la liste des trains trouvées
     */
    public ArrayList<Train> chercherTrain(String nomVilleDeDepart, String nomVilleArrivee, String dateDeDepart) throws Exception;

}
