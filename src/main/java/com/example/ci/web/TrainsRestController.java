package com.example.ci.web;

import com.example.ci.Train;
import com.example.ci.TrainsInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class TrainsRestController {

    TrainsInterface trainsInterface;

    @Autowired
    public TrainsRestController(TrainsInterface trainsInterface){
        this.trainsInterface = trainsInterface;
    }

    @GetMapping("/trains")
    public ArrayList<Train> chercherTrainParLieuDeDepart(@RequestParam("nomVille") String nomVile) throws TrainsException {
        try {
            return trainsInterface.chercherTrainParLieuDeDepart("Paris");
        } catch (Exception e) {
           throw new TrainsException(e.getMessage());
        }
    }

}
