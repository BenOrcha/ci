package com.example.ci.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class TrainsException extends Exception{

    public TrainsException(String message) {
        super(message);
    }

    public TrainsException(){
    }

}
